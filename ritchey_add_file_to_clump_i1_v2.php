<?php
#Name:Ritchey Add File To Clump i1 v2
#Description:Add a file to a clump (clump uses Ritchey Clump v2 format). Returns location of clump pointer file success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'source' (required) is the path to a file to copy into the clump. 'destination' (required) is the path to write the clump pointer file to. 'clump_location' (required) is the path to a clump. 'block_size' (optional) is a number indicating the size in bytes to break the file data into for storage in the clump (default is 20 bytes which, is larger than '9223372036854775807' which, is the max int in PHP on 64bit systems, and is 19 bytes long). 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):source:file:required,destination:file:required,clump_location:directory:required,block_size:number:optional,display_errors:bool:optional
#Content:
if (function_exists('ritchey_add_file_to_clump_i1_v2') === FALSE){
function ritchey_add_file_to_clump_i1_v2($source, $destination, $clump_location, $block_size = NULL, $display_errors = NULL){
	$errors = array();
	if (@is_file($source) === TRUE){
		#Do nothing
	} else {
		$errors[] = "source";
	}
	if (@is_file($destination) === FALSE){
		#Do nothing
	} else {
		$errors[] = "destination";
	}
	if (@is_file($clump_location) === TRUE){
		#Do nothing
	} else {
		@file_put_contents($clump_location, '');
	}
	if ($block_size === NULL){
		$block_size = 20;
	} else if (@is_int($block_size) === TRUE){
		#Do Nothing
	} else {
		$errors[] = "block_size";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		###Read source file block_size at a time, and process each block
		$source_handle = fopen($source, "r");
		while(feof($source_handle) === FALSE) {
			###Read block
			$block = fread($source_handle, $block_size);
			###Base64 encode block
			$block_base64 = base64_encode($block);
			###Add to clump IF NOT ALREADY IN IT
			$block_line_number = 0;
			$clump_handle = fopen($clump_location, "r");
			$check1 = FALSE;
			while($check1 === FALSE) {
				###Read line
				$line = fgets($clump_handle);
				if (feof($clump_handle) !== FALSE){
					$check1 = TRUE;
				}
				if ($check1 === FALSE){
					if (rtrim($line) == $block_base64){
						$check1 = TRUE;
					}
				}
				$block_line_number++;
			}
			fclose($clump_handle);
			$line_break = PHP_EOL;
			file_put_contents($clump_location, "{$block_base64}{$line_break}", FILE_APPEND | LOCK_EX);
			###Add to pointer file
			$line_break = PHP_EOL;
			file_put_contents($destination, "{$block_line_number}{$line_break}", FILE_APPEND | LOCK_EX);
		}
		fclose($source_handle);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('ritchey_add_file_to_clump_i1_v2_format_error') === FALSE){
				function ritchey_add_file_to_clump_i1_v2_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("ritchey_add_file_to_clump_i1_v2_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $destination;
	} else {
		return FALSE;
	}
}
}
?>